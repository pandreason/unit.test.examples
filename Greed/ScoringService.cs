﻿using System.Collections.Generic;
using System.Linq;

namespace Greed.V1
{
    public class ScoringService
    {
        public int Score { get; }

        public ScoringService(IEnumerable<int> dice)
        {
            Score = Evaluate(dice.ToList());
        }

        private int Evaluate(List<int> numberCubes)
        {
            List<int> scoreValues = new List<int>();

            List<IGrouping<int, int>> groups = numberCubes.GroupBy(number => number).ToList();

            if (groups.Count == 3 && groups.All(x => x.Count() == 2)) return 800;
            if (groups.Count == 6 && groups.All(x => x.Count() == 1)) return 1200;

            foreach (var group in groups)
            {
                int maxTally = 0;

                switch (group.Key)
                {
                    case 1:
                        if (group.Count() < 3)
                            scoreValues.Add(group.Count() * 100);
                        else
                        {
                            if (group.Count() > 2)
                                maxTally = 1000;
                            if (group.Count() > 3)
                                maxTally *= 2;
                            if (group.Count() > 4)
                                maxTally *= 2;
                            if (group.Count() > 5)
                                maxTally *= 2;
                            scoreValues.Add(maxTally);
                        }

                        break;
                    case 2:
                        if (group.Count() > 2)
                            maxTally = 200;
                        if (group.Count() > 3)
                            maxTally *= 2;
                        if (group.Count() > 4)
                            maxTally *= 2;
                        if (group.Count() > 5)
                            maxTally *= 2;

                        scoreValues.Add(maxTally);
                        break;
                    case 3:
                        if (group.Count() > 2)
                            maxTally = 300;
                        if (group.Count() > 3)
                            maxTally *= 2;
                        if (group.Count() > 4)
                            maxTally *= 2;
                        if (group.Count() > 5)
                            maxTally *= 2;

                        scoreValues.Add(maxTally);
                        break;
                    case 4:
                        if (group.Count() > 2)
                            maxTally = 400;
                        if (group.Count() > 3)
                            maxTally *= 2;
                        if (group.Count() > 4)
                            maxTally *= 2;
                        if (group.Count() > 5)
                            maxTally *= 2;

                        scoreValues.Add(maxTally);
                        break;
                    case 5:
                        if (group.Count() < 3)
                            scoreValues.Add(group.Count() * 50);
                        else
                        {
                            if (group.Count() > 2)
                                maxTally = 500;
                            if (group.Count() > 3)
                                maxTally *= 2;
                            if (group.Count() > 4)
                                maxTally *= 2;
                            if (group.Count() > 5)
                                maxTally *= 2;

                            scoreValues.Add(maxTally);
                        }

                        break;
                    case 6:
                        if (group.Count() > 2)
                            maxTally = 600;
                        if (group.Count() > 3)
                            maxTally *= 2;
                        if (group.Count() > 4)
                            maxTally *= 2;
                        if (group.Count() > 5)
                            maxTally *= 2;

                        scoreValues.Add(maxTally);
                        break;
                }
            }

            return scoreValues.Aggregate((number, current) => number + current);
        }
    }
}