﻿using System;
using System.Collections.Generic;

namespace Greed.V1.Refactor
{
    public class Referee
    {
        private readonly IScoringService _scoringService;

        public int EvaluateDice(List<int> diceValues)
        {
            if (!_scoringService.AreDiceValid(diceValues)) throw new InvalidOperationException("The dice collection is invalid.");

            return _scoringService.Score(diceValues);
        }

        public Referee(IScoringService scoringService)
        {
            _scoringService = scoringService ?? throw new ArgumentNullException(nameof(scoringService));
        }

        
    }
}