﻿using System.Collections.Generic;

namespace Greed.V1.Refactor
{
    public interface IScoringService
    {
        int Score(List<int> dice);

        bool AreDiceValid(List<int> diceValues);
    }
}