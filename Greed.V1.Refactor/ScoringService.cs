﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Greed.V1.Refactor
{
    public class ScoringService : IScoringService
    {
        #region interface

        public int Score(List<int> dice)
        {
            if (!AreDiceValid(dice))
            {
                throw new InvalidOperationException("The dice collection is invalid.");
            }

            return EvaluateDice(dice);
        }

        public bool AreDiceValid(List<int> diceValues)
        {
            if (diceValues == null) return false;
            if (diceValues.Count > 6) return false;
            if (diceValues.Count < 1) return false;

            return !diceValues.Any(x => x > 6 || x < 1);
        }

        #endregion

        #region Private Methods

        private int EvaluateDice(List<int> numberCubes)
        {
            List<int> scoreValues = new List<int>();

            List<IGrouping<int, int>> groups = numberCubes.GroupBy(number => number).ToList();

            if (ThereAreThreePairsOfDiceWithTheSameValue(groups)) return 800;
            if (ThereIsAOneToSixStraight(groups)) return 1200;

            foreach (var group in groups)
            {
                switch (group.Key)
                {
                    case 1:
                        if (group.Count() <= 2)
                            scoreValues.Add(group.Count() * 100);
                        else
                            scoreValues.Add(CalculateScoreForGroup(group));
                        break;
                    case 5:
                        if (group.Count() <= 2)
                            scoreValues.Add(group.Count() * 50);
                        else
                            scoreValues.Add(CalculateScoreForGroup(group));
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 6:
                        scoreValues.Add(CalculateScoreForGroup(group));
                        break;
                }
            }

            return scoreValues.Aggregate((number, current) => number + current);
        }

        private int CalculateScoreForGroup(IGrouping<int, int> group)
        {
            int scoreForGroup = 0;

            if (group.Count() >= 3)
                scoreForGroup = group.Key == 1 ? 1000 : group.Key *100;

            switch (group.Count())
            {
                case 4:
                    scoreForGroup <<= 1;
                    break;
                case 5:
                    scoreForGroup <<= 2;
                    break;
                case 6:
                    scoreForGroup <<= 3;
                    break;
            }

            return scoreForGroup;
        }

        private bool ThereIsAOneToSixStraight(List<IGrouping<int, int>> groups)
        {
            return groups.Count == 6 && groups.All(x => x.Count() == 1);
        }

        private bool ThereAreThreePairsOfDiceWithTheSameValue(List<IGrouping<int, int>> groups)
        {
            return groups.Count == 3 && groups.All(x => x.Count() == 2);
        }

        #endregion
    }
}