﻿using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using NUnit.Framework;

namespace Greed.V1.Refactor.Tests
{
    [TestFixture]
    public class RefereeTests
    {
        private IScoringService _scoringService;
        private Referee _referee;

        [SetUp]
        public void Setup()
        {
            _scoringService = A.Fake<IScoringService>();
            A.CallTo(() => _scoringService.AreDiceValid(A<List<int>>.Ignored)).ReturnsLazily(objectCall =>
            {
                List<int> diceValues = (List<int>) objectCall.Arguments.FirstOrDefault();

                return new ScoringService().AreDiceValid(diceValues);
            });

            _referee = new Referee(_scoringService);
        }

        [Test]
        public void Prove_referee_throws_exception_when_there_are_more_than_6_parameters()
        {
            InvalidOperationException observedException = Assert.Throws<InvalidOperationException>(() =>
            {
                var diceValues = new List<int> { 1, 2, 3, 4, 5, 6, 6 };

                _referee.EvaluateDice(diceValues);
            });

            Assert.AreEqual("The dice collection is invalid.", observedException.Message);
        }

        [Test]
        public void Prove_referee_throws_exception_when_there_is_a_value_larger_than_six_in_the_parameters()
        {
            InvalidOperationException observedException = Assert.Throws<InvalidOperationException>(() =>
            {
                var diceValues = new List<int> { 10 };

                _referee.EvaluateDice(diceValues);
            });

            Assert.AreEqual("The dice collection is invalid.", observedException.Message);
        }
    }
}
