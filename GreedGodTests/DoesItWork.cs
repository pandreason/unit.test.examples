using System;
using NUnit.Framework;

namespace GreedGod.Tests
{
    public class DoesItWork
    {
        [Test]
        public void Does_it_do_the_thing_13()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                Referee unused = new Referee(1, 2, 3, 4, 5, 6, 6);
            });
            Assert.AreEqual(1200, new Referee(1, 1, 1, 2, 2, 2).HowMuch);
            Assert.AreEqual(100, new Referee(1).HowMuch);
            Assert.AreEqual(50, new Referee(5).HowMuch);
            Assert.AreEqual(200, new Referee(2, 2, 2).HowMuch);
            Assert.AreEqual(1200, new Referee(3,3,3,3,3).HowMuch);
            Assert.AreEqual(1200, new Referee(6,6,6,6).HowMuch);
            Assert.AreEqual(1200, new Referee(6, 4,2,3,1,5).HowMuch);
            Assert.AreEqual(800, new Referee(3, 3, 2, 2, 6, 6).HowMuch);
            Assert.AreEqual(1300, new Referee(1, 1, 1, 3, 3, 3).HowMuch);
            Assert.Throws<InvalidOperationException>(() =>
            {
                Referee unused = new Referee(10);
            });
            Assert.AreEqual(400, new Referee(4, 4, 4).HowMuch);
            Assert.AreEqual(1000, new Referee(1, 1, 1).HowMuch);
            Assert.AreEqual(200, new Referee(1, 1).HowMuch);
            Assert.AreEqual(1400, new Referee(1, 1, 1, 4, 4, 4).HowMuch);
            Assert.AreEqual(1600, new Referee(2,2,2,2,2,2).HowMuch);
        }
    }
}