using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Greed.V1.Tests
{
    public class ScoringServiceTests
    {
        [Test]
        public void Original_tests_from_legacy_project()
        {
            //Cases covered in referee
            Assert.Throws<InvalidOperationException>(() =>
            {
                Referee unused = new Referee(1, 2, 3, 4, 5, 6, 6);
            });

            Assert.Throws<InvalidOperationException>(() =>
            {
                Referee unused = new Referee(10);
            });

            //Cases covered in score service
            Assert.AreEqual(1200, new Referee(1, 1, 1, 2, 2, 2).HowMuch);
            Assert.AreEqual(100, new Referee(1).HowMuch); //
            Assert.AreEqual(50, new Referee(5).HowMuch); //
            Assert.AreEqual(200, new Referee(2, 2, 2).HowMuch); //
            Assert.AreEqual(1200, new Referee(3, 3, 3, 3, 3).HowMuch); //
            Assert.AreEqual(1200, new Referee(6, 6, 6, 6).HowMuch); //
            Assert.AreEqual(1200, new Referee(6, 4, 2, 3, 1, 5).HowMuch); //
            Assert.AreEqual(800, new Referee(3, 3, 2, 2, 6, 6).HowMuch); //
            Assert.AreEqual(1300, new Referee(1, 1, 1, 3, 3, 3).HowMuch);
            Assert.AreEqual(400, new Referee(4, 4, 4).HowMuch); //
            Assert.AreEqual(1000, new Referee(1, 1, 1).HowMuch); //
            Assert.AreEqual(200, new Referee(1, 1).HowMuch); //
            Assert.AreEqual(1400, new Referee(1, 1, 1, 4, 4, 4).HowMuch);
            Assert.AreEqual(1600, new Referee(2, 2, 2, 2, 2, 2).HowMuch); //
        }

        #region special value collections

        [Test]
        public void
            Prove_scoring_service_returns_800_when_there_are_3_groups_of_two_same_values_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                3, 3, 2, 2, 6, 6
            };

            //Assert.AreEqual(800, new ScoringService(3, 3, 2, 2, 6, 6).Score);
            Assert.AreEqual(800, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_1200_when_the_values_one_through_six_are_individually_represented_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                6, 4, 2, 3, 1, 5
            };

            //Assert.AreEqual(1200, new Referee(6, 4, 2, 3, 1, 5).HowMuch);
            Assert.AreEqual(1200, new ScoringService(dice).Score);
        }

        #endregion

        #region values of 1 tests

        [Test]
        public void
            Prove_scoring_service_returns_100_when_there_is_a_single_value_of_one_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                1
            };

            //Assert.AreEqual(100, new Referee(1).HowMuch);
            Assert.AreEqual(100, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_200_when_there_are_two_values_of_one_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                1, 1
            };

            //Assert.AreEqual(200, new Referee(1, 1).HowMuch);
            Assert.AreEqual(200, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_1000_when_there_are_three_values_of_one_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                1, 1, 1
            };

            //Assert.AreEqual(1000, new Referee(1, 1, 1).HowMuch);
            Assert.AreEqual(1000, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_2000_when_there_are_four_values_of_one_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                1, 1, 1, 1
            };

            //This was not represented in the original tests
            Assert.AreEqual(2000, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_4000_when_there_are_five_values_of_one_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                1, 1, 1, 1, 1
            };

            //This was not represented in the original tests
            Assert.AreEqual(4000, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_8000_when_there_are_six_values_of_one_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                1, 1, 1, 1, 1, 1
            };

            //This was not represented in the original tests
            Assert.AreEqual(8000, new ScoringService(dice).Score);
        }

        #endregion

        #region values of 2 tests

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_is_a_single_value_of_two_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                2
            };

            //This was not represented in the original tests
            Assert.AreEqual(0, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_are_two_values_of_two_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                2, 2
            };

            //This was not represented in the original tests
            Assert.AreEqual(0, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_200_when_there_are_three_values_of_two_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                2, 2, 2
            };

            //Assert.AreEqual(200, new Referee(2, 2, 2).HowMuch);
            Assert.AreEqual(200, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_400_when_there_are_four_values_of_two_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                2, 2, 2, 2
            };

            //This was not represented in the original tests
            Assert.AreEqual(400, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_800_when_there_are_five_values_of_two_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                2, 2, 2, 2, 2
            };

            //This was not represented in the original tests
            Assert.AreEqual(800, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_1600_when_there_are_six_values_of_two_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                2, 2, 2, 2, 2, 2
            };

            //Assert.AreEqual(1600, new Referee(2, 2, 2, 2, 2, 2).HowMuch);
            Assert.AreEqual(1600, new ScoringService(dice).Score);
        }

        #endregion

        #region values of 3 tests

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_is_a_single_value_of_three_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                3
            };

            //This was not represented in the original tests
            Assert.AreEqual(0, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_are_two_values_of_three_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                3, 3
            };

            //This was not represented in the original tests
            Assert.AreEqual(0, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_300_when_there_are_three_values_of_three_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                3, 3, 3
            };

            //This was not represented in the original tests
            Assert.AreEqual(300, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_600_when_there_are_four_values_of_three_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                3, 3, 3, 3
            };

            //This was not represented in the original tests
            Assert.AreEqual(600, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_1200_when_there_are_five_values_of_three_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                3, 3, 3, 3, 3
            };

            //Assert.AreEqual(1200, new Referee(3, 3, 3, 3, 3).HowMuch);
            Assert.AreEqual(1200, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_2400_when_there_are_six_values_of_three_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                3, 3, 3, 3, 3, 3
            };

            //This was not represented in the original tests
            Assert.AreEqual(2400, new ScoringService(dice).Score);
        }

        #endregion

        #region values of 4 tests

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_is_a_single_value_of_four_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                4
            };

            //This was not represented in the original tests
            Assert.AreEqual(0, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_are_two_values_of_four_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(0, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_400_when_there_are_three_values_of_four_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                4, 4, 4
            };

            //Assert.AreEqual(400, new Referee(4, 4, 4).HowMuch);
            Assert.AreEqual(400, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_800_when_there_are_four_values_of_four_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(800, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_1600_when_there_are_five_values_of_four_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                4, 4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(1600, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_3200_when_there_are_six_values_of_four_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                4, 4, 4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(3200, new ScoringService(dice).Score);
        }

        #endregion

        #region values of 5 tests

        [Test]
        public void
            Prove_scoring_service_returns_50_when_there_is_a_single_value_of_five_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                5
            };

            //Assert.AreEqual(50, new Referee(5).HowMuch);
            Assert.AreEqual(50, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_are_two_values_of_five_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(100, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_500_when_there_are_three_values_of_five_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(500, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_1000_when_there_are_four_values_of_five_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                5, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(1000, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_2000_when_there_are_five_values_of_five_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                5, 5, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(2000, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_4000_when_there_are_six_values_of_five_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                5, 5, 5, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(4000, new ScoringService(dice).Score);
        }

        #endregion

        #region values of 6 tests

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_is_a_single_value_of_six_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                6
            };

            //This was not represented in the original tests
            Assert.AreEqual(0, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_0_when_there_are_two_values_of_six_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(0, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_600_when_there_are_three_values_of_six_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                6, 6, 6
            };

            //Assert.AreEqual(600, new Referee(6, 6, 6).HowMuch);
            Assert.AreEqual(600, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_1200_when_there_are_four_values_of_six_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                6, 6, 6, 6
            };

            //Assert.AreEqual(1200, new Referee(6, 6, 6, 6).HowMuch);
            Assert.AreEqual(1200, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_2400_when_there_are_five_values_of_six_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                6, 6, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(2400, new ScoringService(dice).Score);
        }

        [Test]
        public void
            Prove_scoring_service_returns_4800_when_there_are_six_values_of_six_in_the_dice_collection()
        {
            List<int> dice = new List<int>
            {
                6, 6, 6, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(4800, new ScoringService(dice).Score);
        }

        #endregion

        #region value combinations tests

        #region single one tests

        [Test]
        public void Prove_scoring_service_returns_100_when_the_dice_collection_is_122334()
        {
            List<int> dice = new List<int>
            {
                1, 2, 2, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(100, new ScoringService(dice).Score);
        }

        #region with twos

        [Test]
        public void Prove_scoring_service_returns_300_when_the_dice_collection_is_122234()
        {
            List<int> dice = new List<int>
            {
                1, 2, 2, 2, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(300, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_500_when_the_dice_collection_is_122224()
        {
            List<int> dice = new List<int>
            {
                1, 2, 2, 2, 2, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(500, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_900_when_the_dice_collection_is_122222()
        {
            List<int> dice = new List<int>
            {
                1, 2, 2, 2, 2, 2
            };

            //This was not represented in the original tests
            Assert.AreEqual(900, new ScoringService(dice).Score);
        }

        #endregion

        #region with threes

        [Test]
        public void Prove_scoring_service_returns_400_when_the_dice_collection_is_123334()
        {
            List<int> dice = new List<int>
            {
                1, 2, 3, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(400, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_700_when_the_dice_collection_is_133334()
        {
            List<int> dice = new List<int>
            {
                1, 3, 3, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(700, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1300_when_the_dice_collection_is_133333()
        {
            List<int> dice = new List<int>
            {
                1, 3, 3, 3, 3, 3
            };

            //This was not represented in the original tests
            Assert.AreEqual(1300, new ScoringService(dice).Score);
        }

        #endregion

        #region with fours

        [Test]
        public void Prove_scoring_service_returns_500_when_the_dice_collection_is_123444()
        {
            List<int> dice = new List<int>
            {
                1, 2, 3, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(500, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_900_when_the_dice_collection_is_134444()
        {
            List<int> dice = new List<int>
            {
                1, 3, 4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(900, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1700_when_the_dice_collection_is_144444()
        {
            List<int> dice = new List<int>
            {
                1, 4, 4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(1700, new ScoringService(dice).Score);
        }

        #endregion

        #region with fives

        [Test]
        public void Prove_scoring_service_returns_150_when_the_dice_collection_is_123566()
        {
            List<int> dice = new List<int>
            {
                1, 2, 3, 5, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(150, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_200_when_the_dice_collection_is_123565()
        {
            List<int> dice = new List<int>
            {
                1, 2, 3, 5, 6, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(200, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_600_when_the_dice_collection_is_123555()
        {
            List<int> dice = new List<int>
            {
                1, 2, 3, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(600, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1100_when_the_dice_collection_is_135555()
        {
            List<int> dice = new List<int>
            {
                1, 3, 5, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(1100, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_2100_when_the_dice_collection_is_155555()
        {
            List<int> dice = new List<int>
            {
                1, 5, 5, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(2100, new ScoringService(dice).Score);
        }

        #endregion

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_700_when_the_dice_collection_is_123666()
        {
            List<int> dice = new List<int>
            {
                1, 2, 3, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(700, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1300_when_the_dice_collection_is_136666()
        {
            List<int> dice = new List<int>
            {
                1, 3, 6, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(1300, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_2500_when_the_dice_collection_is_166666()
        {
            List<int> dice = new List<int>
            {
                1, 6, 6, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(2500, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #region double one tests

        [Test]
        public void Prove_scoring_service_returns_300_when_the_dice_collection_is_112334()
        {
            List<int> dice = new List<int>
            {
                1, 1, 2, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(200, new ScoringService(dice).Score);
        }

        #region with twos

        [Test]
        public void Prove_scoring_service_returns_400_when_the_dice_collection_is_122214()
        {
            List<int> dice = new List<int>
            {
                1, 2, 2, 2, 1, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(400, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_600_when_the_dice_collection_is_122221()
        {
            List<int> dice = new List<int>
            {
                1, 2, 2, 2, 2, 1
            };

            //This was not represented in the original tests
            Assert.AreEqual(600, new ScoringService(dice).Score);
        }

        #endregion

        #region with threes

        [Test]
        public void Prove_scoring_service_returns_500_when_the_dice_collection_is_113334()
        {
            List<int> dice = new List<int>
            {
                1, 1, 3, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(500, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_800_when_the_dice_collection_is_133331()
        {
            List<int> dice = new List<int>
            {
                1, 3, 3, 3, 3, 1
            };

            //This was not represented in the original tests
            Assert.AreEqual(800, new ScoringService(dice).Score);
        }

        #endregion

        #region with fours

        [Test]
        public void Prove_scoring_service_returns_600_when_the_dice_collection_is_113444()
        {
            List<int> dice = new List<int>
            {
                1, 1, 3, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(600, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1000_when_the_dice_collection_is_114444()
        {
            List<int> dice = new List<int>
            {
                1, 1, 4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(1000, new ScoringService(dice).Score);
        }

        #endregion

        #region with fives

        [Test]
        public void Prove_scoring_service_returns_250_when_the_dice_collection_is_113564()
        {
            List<int> dice = new List<int>
            {
                1, 1, 3, 5, 6, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(250, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_300_when_the_dice_collection_is_113565()
        {
            List<int> dice = new List<int>
            {
                1, 1, 3, 5, 6, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(300, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_700_when_the_dice_collection_is_113555()
        {
            List<int> dice = new List<int>
            {
                1, 1, 3, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(700, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1200_when_the_dice_collection_is_115555()
        {
            List<int> dice = new List<int>
            {
                1, 1, 5, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(1200, new ScoringService(dice).Score);
        }

        #endregion

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_800_when_the_dice_collection_is_113666()
        {
            List<int> dice = new List<int>
            {
                1, 1, 3, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(800, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1400_when_the_dice_collection_is_116666()
        {
            List<int> dice = new List<int>
            {
                1, 1, 6, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(1400, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #region tripple one tests

        #region with twos

        [Test]
        public void Prove_scoring_service_returns_1200_when_the_dice_collection_is_122211()
        {
            List<int> dice = new List<int>
            {
                1, 2, 2, 2, 1, 1
            };

            //This was not represented in the original tests
            Assert.AreEqual(1200, new ScoringService(dice).Score);
        }

        #endregion

        #region with threes

        [Test]
        public void Prove_scoring_service_returns_1300_when_the_dice_collection_is_113331()
        {
            List<int> dice = new List<int>
            {
                1, 1, 3, 3, 3, 1
            };

            //This was not represented in the original tests
            Assert.AreEqual(1300, new ScoringService(dice).Score);
        }

        #endregion

        #region with fours

        [Test]
        public void Prove_scoring_service_returns_1400_when_the_dice_collection_is_111444()
        {
            List<int> dice = new List<int>
            {
                1, 1, 1, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(1400, new ScoringService(dice).Score);
        }

        #endregion

        #region with fives

        [Test]
        public void Prove_scoring_service_returns_1500_when_the_dice_collection_is_111555()
        {
            List<int> dice = new List<int>
            {
                1, 1, 1, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(1500, new ScoringService(dice).Score);
        }

        #endregion

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_1600_when_the_dice_collection_is_111666()
        {
            List<int> dice = new List<int>
            {
                1, 1, 1, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(1600, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #region tripple twos tests

        #region with threes

        [Test]
        public void Prove_scoring_service_returns_1300_when_the_dice_collection_is_223332()
        {
            List<int> dice = new List<int>
            {
                2, 2, 3, 3, 3, 2
            };

            //This was not represented in the original tests
            Assert.AreEqual(500, new ScoringService(dice).Score);
        }

        #endregion

        #region with fours

        [Test]
        public void Prove_scoring_service_returns_600_when_the_dice_collection_is_222444()
        {
            List<int> dice = new List<int>
            {
                2, 2, 2, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(600, new ScoringService(dice).Score);
        }

        #endregion

        #region with fives

        [Test]
        public void Prove_scoring_service_returns_700_when_the_dice_collection_is_222555()
        {
            List<int> dice = new List<int>
            {
                2, 2, 2, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(700, new ScoringService(dice).Score);
        }

        #endregion

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_800_when_the_dice_collection_is_222666()
        {
            List<int> dice = new List<int>
            {
                2, 2, 2, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(800, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #region tripple threes tests

        #region with fours

        [Test]
        public void Prove_scoring_service_returns_700_when_the_dice_collection_is_333444()
        {
            List<int> dice = new List<int>
            {
                3, 3, 3, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(700, new ScoringService(dice).Score);
        }

        #endregion

        #region with fives

        [Test]
        public void Prove_scoring_service_returns_800_when_the_dice_collection_is_333555()
        {
            List<int> dice = new List<int>
            {
                3, 3, 3, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(800, new ScoringService(dice).Score);
        }

        #endregion

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_900_when_the_dice_collection_is_333666()
        {
            List<int> dice = new List<int>
            {
                3, 3, 3, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(900, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #region tripple fours tests

        #region with fives

        [Test]
        public void Prove_scoring_service_returns_900_when_the_dice_collection_is_444555()
        {
            List<int> dice = new List<int>
            {
                4, 4, 4, 5, 5, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(900, new ScoringService(dice).Score);
        }

        #endregion

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_1000_when_the_dice_collection_is_444666()
        {
            List<int> dice = new List<int>
            {
                4, 4, 4, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(1000, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #region single five tests

        [Test]
        public void Prove_scoring_service_returns_50_when_the_dice_collection_is_522334()
        {
            List<int> dice = new List<int>
            {
                5, 2, 2, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(50, new ScoringService(dice).Score);
        }

        #region with twos

        [Test]
        public void Prove_scoring_service_returns_250_when_the_dice_collection_is_522234()
        {
            List<int> dice = new List<int>
            {
                5, 2, 2, 2, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(250, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_450_when_the_dice_collection_is_522224()
        {
            List<int> dice = new List<int>
            {
                5, 2, 2, 2, 2, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(450, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_850_when_the_dice_collection_is_522222()
        {
            List<int> dice = new List<int>
            {
                5, 2, 2, 2, 2, 2
            };

            //This was not represented in the original tests
            Assert.AreEqual(850, new ScoringService(dice).Score);
        }

        #endregion

        #region with threes

        [Test]
        public void Prove_scoring_service_returns_350_when_the_dice_collection_is_523334()
        {
            List<int> dice = new List<int>
            {
                5, 2, 3, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(350, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_650_when_the_dice_collection_is_533334()
        {
            List<int> dice = new List<int>
            {
                5, 3, 3, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(650, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1250_when_the_dice_collection_is_533333()
        {
            List<int> dice = new List<int>
            {
                5, 3, 3, 3, 3, 3
            };

            //This was not represented in the original tests
            Assert.AreEqual(1250, new ScoringService(dice).Score);
        }

        #endregion

        #region with fours

        [Test]
        public void Prove_scoring_service_returns_450_when_the_dice_collection_is_523444()
        {
            List<int> dice = new List<int>
            {
                5, 2, 3, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(450, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_850_when_the_dice_collection_is_534444()
        {
            List<int> dice = new List<int>
            {
                5, 3, 4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(850, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1650_when_the_dice_collection_is_544444()
        {
            List<int> dice = new List<int>
            {
                5, 4, 4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(1650, new ScoringService(dice).Score);
        }

        #endregion

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_650_when_the_dice_collection_is_523666()
        {
            List<int> dice = new List<int>
            {
                5, 2, 3, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(650, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1250_when_the_dice_collection_is_536666()
        {
            List<int> dice = new List<int>
            {
                5, 3, 6, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(1250, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_2450_when_the_dice_collection_is_566666()
        {
            List<int> dice = new List<int>
            {
                5, 6, 6, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(2450, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #region double five tests

        [Test]
        public void Prove_scoring_service_returns_100_when_the_dice_collection_is_552334()
        {
            List<int> dice = new List<int>
            {
                5, 5, 2, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(100, new ScoringService(dice).Score);
        }

        #region with twos

        [Test]
        public void Prove_scoring_service_returns_300_when_the_dice_collection_is_522254()
        {
            List<int> dice = new List<int>
            {
                5, 2, 2, 2, 5, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(300, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_500_when_the_dice_collection_is_522225()
        {
            List<int> dice = new List<int>
            {
                5, 2, 2, 2, 2, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(500, new ScoringService(dice).Score);
        }

        #endregion

        #region with threes

        [Test]
        public void Prove_scoring_service_returns_400_when_the_dice_collection_is_553334()
        {
            List<int> dice = new List<int>
            {
                5, 5, 3, 3, 3, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(400, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_700_when_the_dice_collection_is_533335()
        {
            List<int> dice = new List<int>
            {
                5, 3, 3, 3, 3, 5
            };

            //This was not represented in the original tests
            Assert.AreEqual(700, new ScoringService(dice).Score);
        }

        #endregion

        #region with fours

        [Test]
        public void Prove_scoring_service_returns_500_when_the_dice_collection_is_553444()
        {
            List<int> dice = new List<int>
            {
                5, 5, 3, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(500, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_900_when_the_dice_collection_is_554444()
        {
            List<int> dice = new List<int>
            {
                5, 5, 4, 4, 4, 4
            };

            //This was not represented in the original tests
            Assert.AreEqual(900, new ScoringService(dice).Score);
        }

        #endregion

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_700_when_the_dice_collection_is_553666()
        {
            List<int> dice = new List<int>
            {
                5, 5, 3, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(700, new ScoringService(dice).Score);
        }

        [Test]
        public void Prove_scoring_service_returns_1300_when_the_dice_collection_is_556666()
        {
            List<int> dice = new List<int>
            {
                5, 5, 6, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(1300, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #region tripple fives tests

        #region with sixs

        [Test]
        public void Prove_scoring_service_returns_1100_when_the_dice_collection_is_555666()
        {
            List<int> dice = new List<int>
            {
                5, 5, 5, 6, 6, 6
            };

            //This was not represented in the original tests
            Assert.AreEqual(1100, new ScoringService(dice).Score);
        }

        #endregion

        #endregion

        #endregion
    }
}