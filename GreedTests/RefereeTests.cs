﻿using System;
using NUnit.Framework;

namespace Greed.V1.Tests
{
    [TestFixture]
    public class RefereeTests
    {
        [Test]
        public void Prove_referee_throws_exception_when_there_are_more_than_6_parameters()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                Referee unused = new Referee(1, 2, 3, 4, 5, 6, 6);
            });
        }

        [Test]
        public void Prove_referee_throws_exception_when_there_is_a_value_larger_than_six_in_the_parameters()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                Referee unused = new Referee(10);
            });
        }
    }
}
