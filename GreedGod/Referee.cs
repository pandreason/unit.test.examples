﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GreedGod
{
    public class Referee
    {
        public int HowMuch { get; }

        public Referee(params int[] d)
        {
            List<int> values = d.ToList();

            if (values.Count > 6) throw new InvalidOperationException();
            if (values.Any(x => x > 6 || x < 1)) throw new InvalidOperationException();

            ScoreKeeper official = new ScoreKeeper(values);

            HowMuch = official.Score;
        }
    }
}